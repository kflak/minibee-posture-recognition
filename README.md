## MiniBee posture recognition

This app is based on the GestureRecognitionToolkit, and requires the ofxGrt addon to run, as well as ofxOsc. It is intended to run together with SuperCollider sending OSC data from a set of MiniBee accelerometers, but with very small tweaks in the minibee.h file can be rewired to any kind of OSC sender.


## Building and running the example
On Linux, you can build the app by running the following command in terminal:

````
cd THIS_DIRECTORY
make -j4
````

To run the example, run the following in terminal:

````
make run
````

## Using the example

Set the IP address in the OSC-sender (SuperCollider in this case) to that of your computer and set the receiving port number to 5000 (both your host machine and OSC-sender device need to be on the same wireless network).

When you start this example, you should see the graphs on the screen change as you move your device (if not, then check the network settings, IP and port values all match).

To record a training example, simply orientate your senders in a specific orientation (e.g., screen facing up), then press the **r** key to start recording some training examples for that class (the class should default to 1 when you start the example).  Move your phone around for a few seconds to add some variation to the orientation, then press the **r** key again to stop the recording.

Press the **2** number key to switch the class label to 2, move your iOS device to a new orientation (e.g., screen facing down), and press the **r** key again to record some training data for class 2.  Move your phone around for a few seconds to add some variation to the orientation, then press the **r** key again to stop the recording.

Repeat this again for class **3**, holding the phone in a new orientation.

When you have recorded training data for each class (a few hundred training samples per class should be enough), press the **t** key to train a Naive Bayes classification model.

After the model has been trained, realtime classification should immediately start and a new graph should appear that shows the real-time probabilities for detecting each class (i.e, orientation).  If you trained 3 classes, then the graph colors should represent:
- red: class 1
- green: class 2
- blue: class 3

As you move your sensors around, you should see the class probabilities change (with the most likely class being the value closest to 1.0).
