#pragma once

#include "ofMain.h"
#include "ofxGrt.h"
#include "ofxOsc.h"

class MiniBee{
    public:
        MiniBee(){

            newData = false;

        }

        ~MiniBee(){

        }

        bool setup( int listenerPort, int numMBs_, int mbIdOffset_ ){

            oscReceiver.setup( listenerPort );
            mbIdOffset = mbIdOffset_;
            numMBs = numMBs_;
            newData = false;
            numDimensions = numMBs * 3;
            acc.resize( numDimensions, 0 );

            return true;
        }

        bool update(){

            newData = false;
            ofxOscMessage msg;
            while( oscReceiver.hasWaitingMessages() ){
                if( oscReceiver.getNextMessage( msg ) ){
                    if( msg.getAddress() == "/mb/data" ){
                        newData = true;
                        int index = msg.getArgAsInt(0);
                        index -= mbIdOffset;
                        acc[index*3+0] = msg.getArgAsFloat(1);
                        acc[index*3+1] = msg.getArgAsFloat(2);
                        acc[index*3+2] = msg.getArgAsFloat(3);
                    }
                }
            }
            return true;
        }

        bool draw(float x,float y,float w,float h){

            return true;
        }

        bool getNewDataReady() const {
            return newData;
        }

        GRT::VectorFloat getAcc() const {
            return acc;
        }


    protected:
        bool newData;
        int mbIdOffset = 9;
        int numMBs = 6;
        int numDimensions = numMBs * 3;
        GRT::VectorFloat acc;
        ofxOscReceiver oscReceiver;
};
