#pragma once

#include "ofMain.h"
#include "ofxGrt.h"
#include "minibee.h"

//State that we want to use the GRT namespace
using namespace GRT;

#define HOST "localhost"
#define PORT 57120
#define RECEIVE_PORT 5000
#define NUM_MBS 2
#define NUM_POSES 8
#define MB_ID_OFFSET 9

class ofApp : public ofBaseApp{

public:
    void setup();
    void update();
    void draw();

    void keyPressed  (int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    
    //Create some variables for the demo
    ClassificationData trainingData;      		//This will store our training data
    GestureRecognitionPipeline pipeline;        //This is a wrapper for our classifier and any pre/post processing modules 
    bool record;                                //This is a flag that keeps track of when we should record training data
    bool drawInfo;
    UINT trainingClassLabel;                    //This will hold the current label for when we are training the classifier
    string infoText;                            //This string will be used to draw some info messages to the main app window
    ofTrueTypeFont largeFont;
    ofTrueTypeFont smallFont;
    MiniBee minibee;
    ofxGrtTimeseriesPlot accDataPlot;
    ofxGrtTimeseriesPlot predictionPlot;
    ofxOscSender oscsender;
    int prevPose;

};
